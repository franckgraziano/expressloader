ExpressLoader Software 

Easy upload your .hex to your microcontroller m328P or m328PB (Arduino Nano). Inspired by xLoader software.

![image info](ExpressLoader-v1.0-view.png)

- v1.0
- Release 2024-06-01

- Install driver for your Aduino CH340G (Windows will detect and install automatically the good driver). 
- Launch EvenMidiSoftware exe
- Select your mcu (m328p or m328pb).
- Select your USB Comp Port (it's updated every 500ms when the select box is closed). Check the COM available (maybe no one) Close the select box, plug your arduino on USB port
Then open again => the port COMX should appear.
- Select the Baudrate (57600 for m328p, 115200 for m328pb)
- Select your firmware
- Press the button Upload Firmware
- the Console will show you what happened
- If everything is OK at the end you will see a Thank You message 
- If you have trouble the console will show you what's wrong (problem connection (PORT/Baudrate problem) , bad MCU)

This program uses the same avrdude.exe/avrdude.conf v7.2 that is delivered with Arduino Library MiniCore that handle 328P 328PB

Good upload :)
Franck Graziano 